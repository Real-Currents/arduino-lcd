#include <Arduino.h>
#include <Eventually.h>
#include <LiquidCrystal.h>

const int switchPin = 6;
const int buttonPin = 6;
int switchState = 0;
int prevSwitchState = 0;
int reply;

EvtManager mgr;
EvtListener * buttonListener;
bool buttonHandler(void *evt);
bool report(EvtListener *);
bool reportState();
bool reset();

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
    lcd.begin(16,12);
    //pinMode(switchPin, INPUT);
    pinMode(buttonPin, INPUT);

    buttonListener = new EvtPinListener(buttonPin, 10, HIGH, (EvtAction) buttonHandler);
//    buttonListener = new EvtPinListener(buttonPin, (EvtAction) buttonHandler);
    mgr.addListener(buttonListener);
//    reset();
}

bool buttonHandler(void *evt) {
    int *data = static_cast<int *>(static_cast<EvtContext *>(evt)->data);
    char dataSize[4];
    char dataString[sizeof(data)][256];
    itoa(sizeof(data), dataSize, 10);
  
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("# of Inputs: ");
    lcd.print(dataSize);
    delay(1000);

    lcd.clear();
    lcd.setCursor(0, 0);
    for (int d=0; d < sizeof(data); d++) {
        itoa(data[d], dataString[d], 10);
        lcd.setCursor(0, d);
        lcd.print("Input ");
        lcd.print(d);
        lcd.print(": ");
        lcd.print(dataString[d]);
    }

//    lcd.print("Button Pressed: ");
//    lcd.setCursor(0, 1);
//    lcd.print("Iteration ");
//    lcd.print(switchState);
}

bool report(EvtListener * evt) {

    switchState += digitalRead(switchPin);

    lcd.clear();
    delay(1000);
    
    lcd.setCursor(0, 0);
    lcd.print("Button Pressed: ");
    lcd.setCursor(0, 1);
    //lcd.print(evt->data);
    lcd.print("Iteration ");
    lcd.print(switchState);
//    mgr.removeListener(buttonListener);
//    buttonListener = new EvtPinListener(buttonPin, (EvtAction) reset);
//    mgr.addListener(new EvtTimeListener(1500, true, (EvtAction) reportState));

    return false;
}

bool reportState() {

    if (switchState != prevSwitchState) {
//        if (switchState == LOW) {
            reply = random(100) % 8;
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print("The ball says: ");
            lcd.setCursor(0, 1);
            lcd.print(reply);
            lcd.print(" -> ");

            switch (reply) {
                case 1:
                    lcd.print("Most likely");
                    break;
                case 2:
                    lcd.print("Certainly");
                    break;
                case 3:
                    lcd.print("Outlook good");
                    break;
                case 4:
                    lcd.print("Unsure");
                    break;
                case 5:
                    lcd.print("Ask again");
                    break;
                case 6:
                    lcd.print("Doubtful");
                    break;
                case 7:
                    lcd.print("Nope");
                    break;
                default:
                    lcd.print("Yes");
                    break;
            }
//        }
    }

    mgr.addListener(buttonListener);

    prevSwitchState = switchState;

    return false;
}

bool reset() {
    lcd.clear();
    delay(1000);
    
    lcd.setCursor(0, 0);
    lcd.print("Push Button To");
    lcd.setCursor(0, 1);
    lcd.print("Begin");
//    mgr.removeListener(buttonListener);
//    buttonListener = new EvtPinListener(buttonPin, (EvtAction) report);
//    mgr.addListener(buttonListener);

    return false;
}


//USE_EVENTUALLY_LOOP(mgr)
void loop() {
    mgr.loopIteration();
}
